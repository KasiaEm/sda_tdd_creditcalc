package com.sda.credit.enums;

public enum ResultType {
    DENIAL, LOW_RISK, HIGH_RISK, MEDIUM_RISK
}
