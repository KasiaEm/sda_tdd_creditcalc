package com.sda.credit;

public class IncorrectIDFormatException extends RuntimeException{
    public IncorrectIDFormatException(){
        super("Incorrect ID format.");
    }
}
