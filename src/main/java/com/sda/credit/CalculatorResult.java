package com.sda.credit;

import com.sda.credit.enums.ResultType;

import java.math.BigDecimal;

public final class CalculatorResult {
    private final ResultType type;
    private final BigDecimal amount;
    private final Integer period;

    public CalculatorResult(ResultType type, BigDecimal amount, Integer period) {
        this.type = type;
        this.amount = amount;
        this.period = period;
    }

    public ResultType getType() {
        return type;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Integer getPeriod() {
        return period;
    }

}
