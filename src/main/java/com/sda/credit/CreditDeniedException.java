package com.sda.credit;

public class CreditDeniedException extends RuntimeException{
    public CreditDeniedException(){
        super("Credit denied.");
    }
}
