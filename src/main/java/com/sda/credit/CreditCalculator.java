package com.sda.credit;

import com.sda.credit.enums.ResultType;
import com.sda.credit.service.CreditHistoryService;
import com.sda.model.Person;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;
import java.util.Optional;

public class CreditCalculator {
    private CalculatorResult calculatorResult;
    private CreditHistoryService creditHistoryService;
    private Person person;

    public CreditCalculator(Person person, CreditHistoryService creditHistoryService) {
        this.person = person;
        this.creditHistoryService = creditHistoryService;
    }

    public CalculatorResult getCalculatorResult() {
        return calculatorResult;
    }

    public Person getPerson() {
        return person;
    }

    public BigDecimal checkDebt() throws IOException {
        Optional<BigDecimal> result = creditHistoryService.findDebtor(person.getId());
        return result.isPresent() ? result.get() : BigDecimal.ZERO;
    }

    public BigDecimal calculateInstallment() throws IOException {
        if (calculatorResult == null) {
            calculateCredit();
        }
        // credit denied, there's no point in calculating installment
        if (calculatorResult != null && calculatorResult.getType().equals(ResultType.DENIAL)) {
            throw new CreditDeniedException();
        }

        BigDecimal periodWrap = BigDecimal.valueOf(calculatorResult.getPeriod());
        BigDecimal amount = calculatorResult.getAmount();
        return amount
                .divide(periodWrap)
                .divide(BigDecimal.valueOf(12))
                .multiply(BigDecimal.valueOf(1.2));
    }

    public CalculatorResult calculateCredit() throws IOException {
        BigDecimal income = person.getIncome();
        int age = Period.between(person.getBirtDate(), LocalDate.now()).getYears();
        BigDecimal debt = checkDebt();
        ResultType resultType = null;
        Integer period = null;
        BigDecimal amount = null;

        // checking denial result type
        if (isDenial(age, income, debt)) {
            calculatorResult = new CalculatorResult(ResultType.DENIAL, null, null);
            return calculatorResult;
        }

        // checking other result types
        if (isHighRisk(age, income)) {
            resultType = ResultType.HIGH_RISK;
        } else if (isMediumRisk(age, income)) {
            resultType = ResultType.MEDIUM_RISK;
        } else {
            resultType = ResultType.LOW_RISK;
        }

        // checking period
        if (age < 50) {
            period = 30;
        } else {
            period = 80 - age;
        }

        // calculating credit amount
        amount = income.multiply(BigDecimal.valueOf(period)).multiply(BigDecimal.valueOf(0.5 * 12));

        calculatorResult = new CalculatorResult(resultType, amount, period);
        return calculatorResult;
    }

    private boolean isDenial(int age, BigDecimal income, BigDecimal debt) {
        BigDecimal debtLimit = income.multiply(BigDecimal.valueOf(0.4 * 12 * 15));
        // if age > 80, age <18, income < 100 or debt > debLimit
        return age > 80 || age < 18 || income.compareTo(BigDecimal.valueOf(100)) == -1 || debt.compareTo(debtLimit) == 1;
    }

    private boolean isHighRisk(int age, BigDecimal income) {
        // if age > 75 or income < 500
        return age > 75 || income.compareTo(BigDecimal.valueOf(500)) == -1;
    }

    private boolean isMediumRisk(int age, BigDecimal income) {
        // if age > 50 or income < 1500
        return age > 50 || income.compareTo(BigDecimal.valueOf(1500)) == -1;
    }
}
