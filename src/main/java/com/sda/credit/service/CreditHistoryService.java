package com.sda.credit.service;

import com.sda.credit.IncorrectIDFormatException;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;

public class CreditHistoryService {
    public static final String DEBTORS_DATA = "debtors-data.csv";

    public CreditHistoryService() {
    }

    public Optional<BigDecimal> findDebtor(String id) throws IOException {
        return Files.readAllLines(Paths.get(DEBTORS_DATA))
                .stream()
                .map(line -> line.split(";"))
                .filter(splittedLine -> splittedLine[0].equals(id))
                .map(selectedLine -> new BigDecimal(selectedLine[1]))
                .findFirst();
    }

    private void checkUserId(String id) {
        boolean isNumber = true;
        char[] characters = id.toCharArray();
        for (int i = 0; i < characters.length; i++) {
            if (!Character.isDigit(characters[i])) {
                isNumber = false;
                break;
            }
        }
        if (!isNumber || id.length() != 11) {
            throw new IncorrectIDFormatException();
        }
    }
}
