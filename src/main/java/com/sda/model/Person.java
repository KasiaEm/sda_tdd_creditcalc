package com.sda.model;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;

public class Person {
    private String id;
    private String name;
    private String familyName;
    private LocalDate birtDate;
    private BigDecimal income;

    public Person() {

    }

    public Person(String id, String name, String familyName, LocalDate birtDate, BigDecimal income) {
        this.id = id;
        this.name = name;
        this.familyName = familyName;
        this.birtDate = birtDate;
        this.income = income;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public LocalDate getBirtDate() {
        return birtDate;
    }

    public void setBirtDate(LocalDate birtDate) {
        this.birtDate = birtDate;
    }

    public BigDecimal getIncome() {
        return income;
    }

    public void setIncome(BigDecimal income) {
        this.income = income;
    }
}
