package com.sda.credit;

import com.sda.credit.enums.ResultType;
import com.sda.credit.service.CreditHistoryService;
import com.sda.model.Person;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Optional;

import static com.sda.credit.Constants.*;
import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CreditCalculatorTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Mock
    CreditHistoryService creditHistoryService;

    @Before
    public void setup() throws IOException {
        when(creditHistoryService.findDebtor(IO_EXCEPTION)).thenThrow(new IOException());
        when(creditHistoryService.findDebtor(DEBTOR_NOT_EXISTS)).thenReturn(Optional.empty());
        when(creditHistoryService.findDebtor(DEBTOR_WRONG_FORMAT_DIGITS)).thenThrow(IncorrectIDFormatException.class);
        when(creditHistoryService.findDebtor(DEBTOR_WRONG_FORMAT_LENGTH)).thenThrow(IncorrectIDFormatException.class);
        when(creditHistoryService.findDebtor(DEBTOR_1000)).thenReturn(Optional.of(BigDecimal.valueOf(1000)));
        when(creditHistoryService.findDebtor(DEBTOR_1000000)).thenReturn(Optional.of(BigDecimal.valueOf(1000000)));
    }

    @Test
    public void checkDebtIOException() throws IOException {
        thrown.expect(IOException.class);
        Person person = new Person(IO_EXCEPTION, null, null, null, null);
        CreditCalculator creditCalculator = new CreditCalculator(person, creditHistoryService);
        creditCalculator.checkDebt();
    }

    @Test
    public void checkDebtIncorrectIDFormatDigits() throws IOException {
        thrown.expect(IncorrectIDFormatException.class);
        Person person = new Person(DEBTOR_WRONG_FORMAT_DIGITS, null, null, null, null);
        CreditCalculator creditCalculator = new CreditCalculator(person, creditHistoryService);
        creditCalculator.checkDebt();
    }

    @Test
    public void checkDebtIncorrectIDFormatLength() throws IOException {
        thrown.expect(IncorrectIDFormatException.class);
        Person person = new Person(DEBTOR_WRONG_FORMAT_LENGTH, null, null, null, null);
        CreditCalculator creditCalculator = new CreditCalculator(person, creditHistoryService);
        creditCalculator.checkDebt();
    }

    @Test
    public void checkDebt1000() throws IOException {
        Person person = new Person(DEBTOR_1000, null, null, null, null);
        CreditCalculator creditCalculator = new CreditCalculator(person, creditHistoryService);
        BigDecimal debt = creditCalculator.checkDebt();
        assertThat(debt).isEqualByComparingTo(BigDecimal.valueOf(1000));
    }

    @Test
    public void calculateInstallment() throws IOException {
        Person person = new Person(DEBTOR_NOT_EXISTS, null, null, AGE_25, INCOME_3000);
        CreditCalculator creditCalculator = new CreditCalculator(person, creditHistoryService);
        assertThat(creditCalculator.calculateInstallment().compareTo(BigDecimal.valueOf(1800))).isEqualTo(0);
    }

    @Test
    public void calculateCreditDenial() throws IOException {
        // DENIAL, other fields must be null if
        // income < 100
        Person incomeLT100 = new Person(DEBTOR_NOT_EXISTS, null, null, AGE_25, INCOME_90);
        CreditCalculator ccIncomeLT100 = new CreditCalculator(incomeLT100, creditHistoryService);
        // age > 80
        Person ageGT80 = new Person(DEBTOR_NOT_EXISTS, null, null, AGE_81, INCOME_3000);
        CreditCalculator ccAgeGT80 = new CreditCalculator(ageGT80, creditHistoryService);
        // age < 18
        Person ageLT18 = new Person(DEBTOR_NOT_EXISTS, null, null, AGE_17, INCOME_3000);
        CreditCalculator ccAgeLT18 = new CreditCalculator(ageLT18, creditHistoryService);
        // debt > 0.4*income*12*15
        Person hugeDebt = new Person(DEBTOR_1000000, null, null, AGE_25, INCOME_3000);
        CreditCalculator ccHugeDdebt = new CreditCalculator(hugeDebt, creditHistoryService);

        //assertions
        // income < 100
        CalculatorResult crIncomeLT100 = ccIncomeLT100.calculateCredit();
        assertThat(crIncomeLT100.getType()).isEqualToComparingFieldByFieldRecursively(ResultType.DENIAL);
        assertThat(crIncomeLT100.getAmount()).isNull();
        // age > 80
        CalculatorResult crAgeGT80 = ccAgeGT80.calculateCredit();
        assertThat(crAgeGT80.getType()).isEqualToComparingFieldByFieldRecursively(ResultType.DENIAL);
        assertThat(crAgeGT80.getAmount()).isNull();
        // age < 18
        CalculatorResult crAgeLT18 = ccAgeLT18.calculateCredit();
        assertThat(crAgeLT18.getType()).isEqualToComparingFieldByFieldRecursively(ResultType.DENIAL);
        assertThat(crAgeLT18.getAmount()).isNull();
        // debt > 0.4*income*12*15
        CalculatorResult crHugeDebt = ccHugeDdebt.calculateCredit();
        assertThat(crHugeDebt.getType()).isEqualToComparingFieldByFieldRecursively(ResultType.DENIAL);
        assertThat(crHugeDebt.getAmount()).isNull();

    }

    @Test
    public void calculateCreaditHighRisk() throws IOException {
        // HIGH_RISK if
        // 75 < age <= 80
        Person ageGT75 = new Person(DEBTOR_NOT_EXISTS, null, null, AGE_76, INCOME_3000);
        CreditCalculator ccAgeGT75 = new CreditCalculator(ageGT75, creditHistoryService);
        // 100 <= income < 500
        Person incomeLT500 = new Person(DEBTOR_NOT_EXISTS, null, null, AGE_25, INCOME_400);
        CreditCalculator ccIncomeLt500 = new CreditCalculator(incomeLT500, creditHistoryService);


        // assertions
        // 75 < age <= 80
        CalculatorResult crAgeGT75 = ccAgeGT75.calculateCredit();
        assertThat(crAgeGT75.getType()).isEqualToComparingFieldByFieldRecursively(ResultType.HIGH_RISK);
        assertThat(crAgeGT75.getAmount()).isNotNull();
        // 100 <= income < 500
        CalculatorResult crIncomeLT500 = ccIncomeLt500.calculateCredit();
        assertThat(crIncomeLT500.getType()).isEqualToComparingFieldByFieldRecursively(ResultType.HIGH_RISK);
        assertThat(crIncomeLT500.getAmount()).isNotNull();
    }

    @Test
    public void calculateCreditMediumRisk() throws IOException {
        // MEDIUM_RISK if
        // 50 < age <= 75
        Person ageGT50 = new Person(DEBTOR_NOT_EXISTS, null, null, AGE_60, INCOME_3000);
        CreditCalculator ccAgeGT50 = new CreditCalculator(ageGT50, creditHistoryService);
        // 500 <= income < 1500
        Person incomeLT1500 = new Person(DEBTOR_NOT_EXISTS, null, null, AGE_25, INCOME_1300);
        CreditCalculator ccIncomeLT1500 = new CreditCalculator(incomeLT1500, creditHistoryService);

        // assertions
        // 50 < age <= 75
        CalculatorResult crAgeGT50 = ccAgeGT50.calculateCredit();
        assertThat(crAgeGT50.getType()).isEqualToComparingFieldByFieldRecursively(ResultType.MEDIUM_RISK);
        assertThat(crAgeGT50.getAmount()).isNotNull();
        // 500 <= income < 1500
        CalculatorResult crIncomeLT1500 = ccIncomeLT1500.calculateCredit();
        assertThat(crIncomeLT1500.getType()).isEqualToComparingFieldByFieldRecursively(ResultType.MEDIUM_RISK);
        assertThat(crIncomeLT1500.getAmount()).isNotNull();

    }

    @Test
    public void calculateCreditLowRisk() throws IOException {
        // LOW_RISK if
        // 18 < age <= 50, 1500 <= income
        Person person = new Person(DEBTOR_NOT_EXISTS, null, null, AGE_25, INCOME_3000);
        CreditCalculator cc = new CreditCalculator(person, creditHistoryService);

        // assertions
        // 18 < age <= 50, 1500 <= income
        CalculatorResult cr = cc.calculateCredit();
        assertThat(cr.getType()).isEqualToComparingFieldByFieldRecursively(ResultType.LOW_RISK);
        assertThat(cr.getAmount()).isNotNull();
    }

    @Test
    public void calculateCreditPeriod() throws IOException {
        // period = 30 if
        // age < 50
        Person ageLT50 = new Person(DEBTOR_NOT_EXISTS, null, null, AGE_25, INCOME_3000);
        CreditCalculator ccAgeLT50 = new CreditCalculator(ageLT50, creditHistoryService);
        // period < 30 if
        //age >= 50
        Person ageGT50 = new Person(DEBTOR_NOT_EXISTS, null, null, AGE_60, INCOME_3000);
        CreditCalculator ccAgeGT50 = new CreditCalculator(ageGT50, creditHistoryService);

        //assertions
        // age < 50
        CalculatorResult crAgeLT50 = ccAgeLT50.calculateCredit();
        assertThat(crAgeLT50.getPeriod()).isEqualTo(30);
        //age >= 50
        CalculatorResult crAgeGT50 = ccAgeGT50.calculateCredit();
        assertThat(crAgeGT50.getPeriod()).isEqualTo(80-60);
    }

    @Test
    public void calculateCreditAmount() throws IOException {
        // amount = 540 000 if
        // income = 3000, age = 25
        Person person =  new Person(DEBTOR_1000, null, null, AGE_25, INCOME_3000);
        CreditCalculator cc = new CreditCalculator(person, creditHistoryService);

        //assertions
        CalculatorResult cr = cc.calculateCredit();
        assertThat(cr.getAmount().compareTo(BigDecimal.valueOf(540000))).isEqualTo(0);
    }
}