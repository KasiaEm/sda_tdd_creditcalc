package com.sda.credit;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;

public class Constants {
    // logins
    public final static String DEBTOR_NOT_EXISTS = "80070756234";
    public final static String DEBTOR_WRONG_FORMAT_DIGITS = "8007075a234";
    public final static String DEBTOR_WRONG_FORMAT_LENGTH = "8007034";
    public final static String DEBTOR_1000 = "80070700001";
    public final static String DEBTOR_1000000 = "80070700002";
    public final static String IO_EXCEPTION = "00000000000";
    // incomes
    public final static BigDecimal INCOME_3000 = BigDecimal.valueOf(3000);
    public final static BigDecimal INCOME_1300 = BigDecimal.valueOf(1300);
    public final static BigDecimal INCOME_400 = BigDecimal.valueOf(400);
    public final static BigDecimal INCOME_90 = BigDecimal.valueOf(90);
    // birthdates
    public final static LocalDate AGE_81 = LocalDate.now().minus(Period.ofYears(81));
    public final static LocalDate AGE_76 = LocalDate.now().minus(Period.ofYears(76));
    public final static LocalDate AGE_60 = LocalDate.now().minus(Period.ofYears(60));
    public final static LocalDate AGE_25 = LocalDate.now().minus(Period.ofYears(25));
    public final static LocalDate AGE_17 = LocalDate.now().minus(Period.ofYears(17));
}
